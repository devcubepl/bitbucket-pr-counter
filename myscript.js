var wide = false;

function renderCircle(prHref, nb) {
	var className = wide ? ' chpbbprnumbers_full' : '';
	$(prHref).append('<div class="chpbbprnumbers'+className+'">'+nb+'</div>');
}

function main() {
	var url = window.location.pathname;
	var reNotAccount = /^\/account/;

	var hQLLi = $('div[role=group]', document.body);
	wide = hQLLi.width() > 80;

	if (!reNotAccount.exec(url)) {
		var ahrefs = $('a', hQLLi);

		if (ahrefs.length > 1) {
			var prHref = ahrefs[4];
			var apiUrl = prHref.href
				.replace('https://bitbucket.org', 'https://bitbucket.org/!api/2.0/repositories')
				.replace('pull-requests', 'pullrequests');
			apiUrl += '?fields=size&pagelen=1';

			fetch(apiUrl, {credentials: 'same-origin'}).then(function(response) {
				return response.text();
			  }).then(function(textResponse){
					if (textResponse.length > 6) {
						var nb = JSON.parse(textResponse);

						if (nb.size !== false) {
							renderCircle(prHref, nb.size);
						} else {
							console.warn('BitBucket PR Counter: No "size" value in the response');
						}
					}
			  });
		}
	}
}

document.body.onload = main;